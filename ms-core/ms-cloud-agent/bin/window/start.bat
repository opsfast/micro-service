@echo off & setlocal enabledelayedexpansion

title ms-client

% 启动 %
echo Starting ...

java -Xms256m -Xmx256m -XX:MaxPermSize=64M -jar ..\..\ms-cloud-agent-2.0.1.jar

:end
pause