package com.app.info.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.ms.monitor.MsCloudMonitorApplication;
import com.netflix.appinfo.ApplicationInfoManager;
import com.netflix.appinfo.DataCenterInfo;
import com.netflix.appinfo.EurekaInstanceConfig;
import com.netflix.discovery.DiscoveryClient;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameSpringBeanUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MsCloudMonitorApplication.class)
@WebAppConfiguration
public class DetailTest {
	
	@Test
	public void test() {
		DiscoveryClient discoveryClient = FrameSpringBeanUtil.getBean(DiscoveryClient.class);
		ApplicationInfoManager appManager = discoveryClient.getApplicationInfoManager();
		EurekaInstanceConfig eic = appManager.getEurekaInstanceConfig();
		System.out.println("EurekaInstanceConfig - instanceId: " + eic.getInstanceId());
		System.out.println("EurekaInstanceConfig - appname: " + eic.getAppname());
		DataCenterInfo dci = eic.getDataCenterInfo();
		System.out.println("DataCenterInfo - name: " + dci.getName());
		/*System.out.println("ApplicationInfoManager: " + FrameJsonUtil.toString(appManager));
		System.out.println("discoveryClient: " + FrameJsonUtil.toString(discoveryClient));*/
	}
}
