package com.module.api.service;

import java.util.List;
import java.util.Map;

import com.system.handle.model.ResponseFrame;

public interface ApiPrjApiService {

	public ResponseFrame saveBatch(String code, List<Map<String, String>> details);
	/**
	 * 批量保存项目API的为未使用
	 * @param code
	 * @return
	 */
	public ResponseFrame updateNotUse(String code);

}
