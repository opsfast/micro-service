package com.module.admin.prj.service;

import org.springframework.stereotype.Component;

import com.module.admin.prj.pojo.PrjOptimizeLog;
import com.system.handle.model.ResponseFrame;

/**
 * prj_optimize_log的Service
 * @author autoCode
 * @date 2018-05-31 15:51:51
 * @version V1.0.0
 */
@Component
public interface PrjOptimizeLogService {
	
	/**
	 * 保存
	 * @param prjOptimizeLog
	 * @return
	 */
	public void save(PrjOptimizeLog prjOptimizeLog);
	
	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	public PrjOptimizeLog get(Integer id);

	/**
	 * 分页获取对象
	 * @param prjOptimizeLog
	 * @return
	 */
	public ResponseFrame pageQuery(PrjOptimizeLog prjOptimizeLog);
	
	/**
	 * 根据id删除对象
	 * @param id
	 * @return
	 */
	public ResponseFrame delete(Integer id);

	public PrjOptimizeLog getDtl(Integer id);

	public ResponseFrame deleteAll();
}