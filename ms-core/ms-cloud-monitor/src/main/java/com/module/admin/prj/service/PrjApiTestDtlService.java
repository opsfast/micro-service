package com.module.admin.prj.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.module.admin.prj.pojo.PrjApiTestDtl;
import com.system.handle.model.ResponseFrame;

/**
 * prj_api_test_dtl的Service
 * @author autoCode
 * @date 2018-03-08 10:59:40
 * @version V1.0.0
 */
@Component
public interface PrjApiTestDtlService {
	
	/**
	 * 保存或修改
	 * @param prjApiTestDtl
	 * @return
	 */
	public ResponseFrame saveOrUpdate(PrjApiTestDtl prjApiTestDtl);
	
	/**
	 * 根据patId获取对象
	 * @param patId
	 * @param path 
	 * @return
	 */
	public PrjApiTestDtl get(Integer patId, String path);

	/**
	 * 分页获取对象
	 * @param prjApiTestDtl
	 * @return
	 */
	public ResponseFrame pageQuery(PrjApiTestDtl prjApiTestDtl);
	
	/**
	 * 根据patId删除对象
	 * @param patId
	 * @return
	 */
	public ResponseFrame delete(Integer patId, String path);

	public List<PrjApiTestDtl> findByPatId(Integer patId);

	public void updateStatusByPatId(Integer patId, Integer status);

	public void updateStatus(Integer patId, String path, Integer status,
			String testResult);
}