package com.module.admin.prj.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * prj_ant实体
 * @author autoCode
 * @date 2018-01-30 20:32:37
 * @version V1.0.0
 */
@Alias("prjAnt")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class PrjAnt extends BaseEntity implements Serializable {
	//编号
	private Integer paId;
	//项目编号
	private Integer prjId;
	//名称
	private String name;
	//父编号
	private Integer pid;
	//创建时间
	private Date createTime;
	//图片
	private String img;
	//api内容
	private String apiText;
	
	public Integer getPaId() {
		return paId;
	}
	public void setPaId(Integer paId) {
		this.paId = paId;
	}
	
	public Integer getPrjId() {
		return prjId;
	}
	public void setPrjId(Integer prjId) {
		this.prjId = prjId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	
	public String getApiText() {
		return apiText;
	}
	public void setApiText(String apiText) {
		this.apiText = apiText;
	}
}