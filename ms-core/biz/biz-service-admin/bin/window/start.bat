@echo off & setlocal enabledelayedexpansion

% 启动 %
echo Starting ...

set project.dir=${user.dir}/../../
java -Xms256m -Xmx256m -XX:MaxPermSize=64M -Dproject.dir=%project.dir% -jar ..\..\biz-service-admin-2.0.1.war

:end
pause